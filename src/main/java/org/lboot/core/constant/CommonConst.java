package org.lboot.core.constant;


import org.springframework.stereotype.Component;


@Deprecated
public class CommonConst {
    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_code:";
    /**
     * 二维码 redis key
     */
    public static final String QR_CODE_KEY = "qr_code:";
    /**
     * 验证码有效期（分钟）
     */
    public static final Integer CAPTCHA_EXPIRATION = 2;
    /**
     * 对称加密AES KEY
     */
    @Deprecated
    public static final String AES_KEY = "SpringTestAesKey";
    /**
     * MAX PAGE_SIZE
     */
    public static final long MAX_PAGE_SIZE = 1024 * 1024;

}
