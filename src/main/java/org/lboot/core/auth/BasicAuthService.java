package org.lboot.core.auth;

/**
 * 基础鉴权服务
 * @author kindear
 * @param <T> 用户ID类型
 */
public interface BasicAuthService {
    /**
     * 拓展实现登录操作
     * @return 登录返回值 String
     */
    default Boolean doLogin(String id){
        return false;
    }

    /**
     * 拓展实现登出操作
     * @param id
     * @return 登出操作是否成功
     */
    default Boolean doLogout(String id){
        return false;
    }
}
