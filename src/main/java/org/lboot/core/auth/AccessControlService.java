package org.lboot.core.auth;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kindear
 * 访问控制相关接口
 */
public interface AccessControlService {
    /**
     * 判断当前会话用户是否处于登陆态
     * @return 登录态
     */
    Boolean isLogin();

    /**
     * 判断当前登录用户是否是管理员角色
     * @return 管理员身份验证结果
     */
     default Boolean isAdmin(){
         if (hasRoleKey("admin")){
             return true;
         }
         return false;
     }
    /**
     * 获取当前会话用户拥有的角色列表标识
     * @return list
     */
    List<String> getRoleKeys();

    List<String> getRoleIds();


    /**
     * 获取当前会话用户拥有的权限列表
     * @return list
     */
     List<String> getPerms();

    /**
     * [无需重写]
     * 判断当前会话用户是否拥有某角色
     * @param roleKey 角色标识符
     * @return bool
     */
    default Boolean hasRoleKey(String roleKey){
        List<String> roleKeys = getRoleKeys();
        if (roleKeys.contains(roleKey)){
            return true;
        }
        return false;
    }

    default Boolean hasRoleId(String roleId){
        List<String> roleIds = getRoleIds();
        if (roleIds.contains(roleId)){
            return true;
        }
        return false;
    }
    /**
     * 判断当前会话用户是否拥有某权限
     * @param perm 权限标识
     * @return bool
     */
    default Boolean hasPerm(String perm){
        List<String> perms = getPerms();
        if (perms.contains(perm) || perms.contains("*:*:*")){
            return true;
        }
        return false;
    }


}
