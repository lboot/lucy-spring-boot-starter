package org.lboot.core.auth.anno;

import java.lang.annotation.*;

/**
 * @author kindear
 * 方法注解鉴权拦截  方法执行前 校验当前登录用户角色
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
public @interface CheckRole {
    // 校验角色标识符 -- 默认为管理员
    String value() default "";

    // 或者拥有 某种权限
    String orPerm() default "";
}
