package org.lboot.core.auth;

public interface AuthService extends BasicAuthService, AuthDataService, AccessControlService, RiskAuthService{
}
