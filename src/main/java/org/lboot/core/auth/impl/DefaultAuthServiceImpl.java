package org.lboot.core.auth.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lboot.core.auth.AuthService;

import java.util.List;

@Slf4j
@AllArgsConstructor
public class DefaultAuthServiceImpl implements AuthService{
    @Override
    public Boolean isLogin() {
        return false;
    }

    @Override
    public List<String> getRoleKeys() {
        return null;
    }

    @Override
    public List<String> getRoleIds() {
        return null;
    }

    @Override
    public List<String> getPerms() {
        return null;
    }

    @Override
    public String getUid() {
        return null;
    }

}
