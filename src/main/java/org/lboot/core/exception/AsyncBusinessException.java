package org.lboot.core.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.lboot.core.domain.ErrMsg;
import org.springframework.http.HttpStatus;

import java.util.concurrent.CompletionException;

@EqualsAndHashCode(callSuper = true)
@Data
public class AsyncBusinessException extends CompletionException {
    Integer code;

    ErrMsg errMsg;

    public AsyncBusinessException(){}

    public AsyncBusinessException(HttpStatus status, ErrMsg msg){
        super(msg.getMessage());
        this.errMsg = msg;
        this.code = status.value();
    }
    public AsyncBusinessException(HttpStatus status, String msg){
        super(msg);
        this.errMsg = ErrMsg.build(msg);
        this.code = status.value();
    }
    public AsyncBusinessException(ErrMsg msg){
        super(msg.getMessage());
        this.errMsg = msg;
        this.code = 500;
    }
}
