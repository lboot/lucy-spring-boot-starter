package org.lboot.core.domain;

import cn.hutool.core.date.DateUtil;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author kindear
 * 错误信息定位展示实体类
 */
@Data
@ApiModel(value = "错误信息")
public class ErrMsg {
    // 业务错误码
    Integer statusCode;
    // 错误信息
    String message;
    // 参考地址
    String href;
    // 异常堆栈追踪
    String location;
    // 异常发生时间
    String occurTime;

    public ErrMsg(){
        this.occurTime = DateUtil.now();
    }

    public ErrMsg(String message){
        this.occurTime = DateUtil.now();
        this.message = message;
    }

    public static ErrMsg build(String message){
        return new ErrMsg(message);
    }
}

