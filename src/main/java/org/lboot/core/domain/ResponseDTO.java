package org.lboot.core.domain;

import io.swagger.annotations.ApiModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Slf4j
@ApiModel(value = "响应数据封装")
public class ResponseDTO<T> extends ResponseEntity<T> {


    /**
     * 提取返回数据
     * @return body 信息
     */
    public T getData(){
        return this.getBody();
    }


    public ResponseDTO(HttpStatus httpStatus) {
        super(httpStatus);
    }

    public ResponseDTO(HttpStatus httpStatus, T data){
        super(data,httpStatus);
    }

    public static <T> ResponseDTO<T> succ(){
        return new ResponseDTO<T>(HttpStatus.OK);
    }

    public static <T> ResponseDTO<T> succData(T data){
        return new ResponseDTO<>(HttpStatus.OK, data);
    }

    public static <T> ResponseDTO<T> succMsg(T data){
        return new ResponseDTO<T>(HttpStatus.OK,data);
    }

    public static <T> ResponseDTO<T> wrap(HttpStatus httpStatus){
        return new ResponseDTO<>(httpStatus);
    }

    public static <T> ResponseDTO<T> wrap(HttpStatus httpStatus, T data){
        return new ResponseDTO<T>(httpStatus,data);
    }

}
