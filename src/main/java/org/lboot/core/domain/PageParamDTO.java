package org.lboot.core.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "分页查询数据对象",description = "分页查询数据对象")
public class PageParamDTO {

    @ApiModelProperty(value = "页码(不能为空)", example = "1")
    Integer pageNum=0;


    @ApiModelProperty(value = "每页数量(不能为空)", example = "10")
    Integer pageSize=30;

    /**
     * 依据什么进行排序
     */
    @ApiModelProperty(value = "排序参数",example = "id")
    String sortBy="createdTime";

    /**
     * 排序规则 DESC/AES
     */
    @ApiModelProperty(value = "排序规则",example = "DESC")
    String order="DESC";
}
