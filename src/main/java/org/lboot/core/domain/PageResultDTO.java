package org.lboot.core.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Kindear
 * @param <T>
 */
@Data
public class PageResultDTO<T> {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Integer number;

    /**
     * 每页的数量
     */
    @ApiModelProperty(value = "每页的数量")
    private Integer size;

    /**
     * 总记录数
     */
    @ApiModelProperty(value = "总记录数")
    private Long totalElements;

    /**
     * 总页数
     */
    @ApiModelProperty(value = "总页数")
    private Integer totalPages;

    /**
     * 结果集
     */
    @ApiModelProperty(value = "结果集")
    private List<T> content;

}
