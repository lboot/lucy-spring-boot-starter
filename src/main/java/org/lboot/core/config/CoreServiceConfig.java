package org.lboot.core.config;

import org.lboot.core.auth.AuthService;
import org.lboot.core.auth.impl.DefaultAuthServiceImpl;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

/**
 * @author kindear
 * 服务实现配置
 */
@SpringBootConfiguration
public class CoreServiceConfig {
    @Bean
    @ConditionalOnMissingBean(value = AuthService.class)
    public AuthService authService() {
        return new DefaultAuthServiceImpl();
    }
}
