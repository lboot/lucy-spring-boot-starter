package org.lboot.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@EnableCaching
@SpringBootApplication
@ComponentScan(basePackages = {
        "org.lboot.starter",
        "org.lboot.core"
})
public class LucySpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(LucySpringBootStarterApplication.class, args);
    }

}
