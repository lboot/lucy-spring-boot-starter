package org.lboot.starter.test;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.lboot.core.auth.anno.CheckPerm;
import org.lboot.starter.plugin.rsr.RsrTree;

/**
 * 接口可以用 MOCK形式构建
 */
@RsrTree("MOCK2")
@Api(tags = "MOCK2接口")
public interface MockRsr2 {

    @CheckPerm(value = "rsr:mock:tes1",orRole = "admin")
    @ApiOperation(value = "MOCK-test1")
    public void test1();


}
