package org.lboot.starter.test;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.lboot.core.auth.anno.CheckPerm;
import org.lboot.core.domain.ResponseDTO;
import org.lboot.starter.anno.FormLock;
import org.lboot.starter.plugin.rsr.RsrTree;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RsrTree("测试")
@RequestMapping("test/rsr")
@Api(tags = "资源测试接口")
public class AuthRsrTestController {
    @CheckPerm("rsr:create")
    @PostMapping("create")
    @ApiOperation(value = "资源新建")
    public ResponseDTO<Object> postCreate(){
        return null;
    }
}
