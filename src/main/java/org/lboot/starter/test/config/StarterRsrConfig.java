package org.lboot.starter.test.config;

import lombok.extern.slf4j.Slf4j;
import org.lboot.starter.plugin.rsr.AuthRsrConfig;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class StarterRsrConfig implements AuthRsrConfig {
    @Override
    public String scanPackage() {
        return "org.lboot.starter.test";
    }
}
