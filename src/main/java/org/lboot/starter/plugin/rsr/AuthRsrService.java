package org.lboot.starter.plugin.rsr;

import cn.hutool.core.lang.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author kindear
 * 资源统计服务
 */
@Slf4j
@Service
public class AuthRsrService {
    @Autowired(required = false)
    List<AuthRsrConfig> rsrConfigs;

    /**
     * 根据分组 key 获取资源列表信息
     * @param group
     * @return
     */
    public List<ResTreeNode> rsrGroup(String group){
        Map<String,List<ResTreeNode>> resMap = rsrGroup();
        return resMap.get(group);
    }


    /**
     * 获取资源组信息
     * @return
     */
    public Map<String,List<ResTreeNode>> rsrGroup(){
        Map<String,List<ResTreeNode>> nodes = new HashMap<>();
        if (Validator.isEmpty(rsrConfigs)) return null;
        for (AuthRsrConfig rsrConfig:rsrConfigs){
            Map<String,List<ResTreeNode>> inNodes = rsrConfig.loadRsr();
            for (String inKey:inNodes.keySet()){
                List<ResTreeNode> c = new ArrayList<>();
                if (Validator.isNotEmpty(nodes.get(inKey))){
                    c = nodes.get(inKey);
                }
                c.addAll(inNodes.get(inKey));
                nodes.put(inKey,c);
            }
        }
        return nodes;

    }

    /**
     * 生成并构建资源树
     * @return
     */
    public List<ResTreeNode> rsrTree(List<String> groups){
        Map<String, List<ResTreeNode>> resMap = rsrGroup();
        List<ResTreeNode> ans = new ArrayList<>();
        if (Validator.isEmpty(resMap)) return ans;
        for (String key: resMap.keySet()){
            ResTreeNode node = new ResTreeNode();
            node.setDisabled(true);
            node.setGroup(key);
            node.setLabel(key);
            node.setExt(null);
            node.setValue(key);
            node.setChildren(resMap.get(key));
            if (Validator.isEmpty(groups) || groups.contains(key)){
                ans.add(node);
            }

        }
        return ans;
    }

    public List<ResTreeNode> rsrTree(){
        Map<String, List<ResTreeNode>> resMap = rsrGroup();
        List<ResTreeNode> ans = new ArrayList<>();
        if (Validator.isEmpty(resMap)) return ans;
        for (String key: resMap.keySet()){
            ResTreeNode node = new ResTreeNode();
            node.setDisabled(true);
            node.setGroup(key);
            node.setLabel(key);
            node.setExt(null);
            node.setValue(key);
            node.setChildren(resMap.get(key));
            ans.add(node);

        }
        return ans;
    }

}
