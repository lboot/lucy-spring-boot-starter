package org.lboot.starter.plugin.rsr;

import java.lang.annotation.*;

/**
 * @author kindear
 * 被该注解标记的方法，生成权限树的时候会忽略
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface RsrIgnore {
    String value() default "";
}
