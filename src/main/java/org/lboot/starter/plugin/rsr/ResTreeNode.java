package org.lboot.starter.plugin.rsr;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 资源树节点
 */
@Data
@ApiModel(value = "资源树节点")
public class ResTreeNode {
    @ApiModelProperty("分组")
    String group;
    @ApiModelProperty("标签")
    String label;
    @ApiModelProperty("值")
    String value;
    @ApiModelProperty("禁用")
    Boolean disabled;
    @ApiModelProperty("拓展信息")
    String ext;
    @ApiModelProperty("子节点")
    List<ResTreeNode> children;
}
