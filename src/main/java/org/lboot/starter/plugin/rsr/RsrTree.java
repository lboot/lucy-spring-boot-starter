package org.lboot.starter.plugin.rsr;

import java.lang.annotation.*;

/**
 * @author kindear
 * 权限检查
 * 构建资源树
 * 基于 @CheckRole & @CheckPerm @Api*** 构建综合注解
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RsrTree {
    // 构建权限树分组
    String value() default "DEFAULT";
}
