package org.lboot.starter.provider;

import org.lboot.starter.service.ModuleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class StarterModuleServiceImpl implements ModuleService {

    @Override
    public String moduleName() {
        return "lucy-spring-boot-starter";
    }

    // @TODO 修改 POM
    @Override
    public String moduleVersion() {
        return "v1.1.3";
    }
}
