package org.lboot.starter.auto;

import cn.hutool.core.lang.Validator;
import org.lboot.starter.service.ModuleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.List;

@Slf4j
@Configuration
@ComponentScan(basePackages = {
        "org.lboot.starter",
        "org.lboot.core"
})
public class LucyAutoConfiguration implements EnvironmentAware {
    // 引人模块查询
    @Autowired(required = false)
    List<ModuleService> modules;

    @Override
    public void setEnvironment(Environment environment) {
//        String redisHost = environment.getProperty("spring.redis.host");
//        if (Validator.isEmpty(redisHost)){
//            log.warn(
//                    "\n-------------------------------------\n"
//                            +"RedisCache 未配置，不可使用"
//                            +"\n-------------------------------------"
//            );
//        }
        StringBuilder logStr = new StringBuilder();
        if (Validator.isNotEmpty(modules)){
            for (ModuleService service:modules){
                logStr.append(service.moduleName()).append(" ").append(service.moduleVersion()).append("\n");
            }
        }

        log.info(
                "\n-------------------------------------\n"
                +logStr
                +"-------------------------------------"
        );
    }
}
