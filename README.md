<p align="center">
	<img alt="logo" src="./logo.png" width="150" height="150">
</p>
<h2 align="center" style="margin: 30px 0 30px; font-weight: bold;">Lucy v1.1.2</h2>
<h4 align="center">不断迭代的技术解决方案</h4>


---

## 介绍

**Lucy** 是一系列技术方案的整合框架，也可以视作一个完整项目多个不同模块的拆分，支持按需引入，秉承着高可用的观念，通过对`POM`文件修改完成解决方案的更迭，并提供配套前端方案。同时也支持自定义拓展实现方案，更好适配业务系统。

该框架核心为`lucy-spring-boot-starter`，是多种解决方案的必需依赖。

基于该版本构建的项目有:

| 项目                                                         |
| ------------------------------------------------------------ |
| [LLog: 轻量级日志监控组件，集成管理面板，支持多条件查询检索](https://gitee.com/lboot/LLog) |
| [lucy-jpa: JPA核心依赖，支持自定义审计，快速构建流程](https://gitee.com/lboot/lucy-jpa) |
| [lucy-rbac: 基于SaToken的RBAC的鉴权方案实现，支持细粒度权限管理](https://gitee.com/lboot/lucy-rbac) |




## 准备

在引入任何 `Lucy`系列依赖之前，需要完成`jitpack`镜像仓库的配置。

```xml
<repositories>
        <repository>
            <id>jitpack.io</id>
            <url>https://www.jitpack.io</url>
        </repository>
</repositories>
```



## 集成

### 引入

在`pom`文件中直接引入，`version`与发行版本号一致，此处为`1.1.2`。

```xml
<dependency>
        <groupId>com.gitee.lboot</groupId>
        <artifactId>lucy-spring-boot-starter</artifactId>
        <version>1.1.2</version>
</dependency>
```

引入后自动开启如下功能:

- 跨域请求支持
- 统一异常拦截

### 配置

#### 1. 异常处理

`lucy-spring-boot-starter`存在默认的异常拦截处理策略，默认开启，如果需要关闭，需要在配置文件中声明。

```properties
# 异常处理策略
lucy.exception.handle=false
```

#### 2. 缓存策略

`lucy-spring-boot-starter`集成默认的缓存策略，配置`spring.redis.host`后自动开启`Redis`缓存策略。

```properties
###################### Redis 配置(可选，不配置 RedisCache 不生效) ############################
# Redis数据库索引（默认为0）
spring.redis.database=0
# Redis服务器地址
spring.redis.host=127.0.0.1
# Redis服务器连接端口
spring.redis.port=6379
# Redis服务器连接密码
spring.redis.password=123456
# 连接池最大连接数（使用负值表示没有限制）
spring.redis.jedis.pool.max-active=200
spring.data.redis.repositories.enabled = false
# 连接池中的最大空闲连接
spring.redis.jedis.pool.max-idle=10
# 连接池中的最小空闲连接
spring.redis.jedis.pool.min-idle=0
# 连接超时时间（毫秒）
spring.redis.timeout=2000ms
# 最大等待时间 (毫秒)
spring.redis.jedis.pool.max-wait=-1ms
```



## 功能

### 鉴权服务

#### 使用条件

1. 引入`lucy-spring-boot-starter`

2. 自定义实现`AuthService`或直接引入解决方案

   自定义实现: 继承并实现`AuthService`

   解决方案:

   | 解决方案                                                     |
   | ------------------------------------------------------------ |
   | [lucy-rbac: 基于SaToken的RBAC的鉴权方案实现，支持细粒度权限管理](https://gitee.com/lboot/lucy-rbac) |

#### 服务使用

在需要使用鉴权服务的地方引入

```java
public class DemoCtl {
    @Autowired
    AuthService authService;
}
```

#### 方法列表

| 方法                            | 功能                                         | 返回值   | 备注   |
|:------------------------------| :------------------------------------------- | :------- | :----- |
| getUid                        | 获取登录用户ID                               | String   | 1.0.0+ |
| getUserName                   | 获取登录用户账号                             | String   | 1.0.0+ |
| getNickName                   | 获取登录用户昵称                             | String   | 1.0.0+ |
| getUser                       | 获取登录用户信息                             | BaseUser | 1.0.0+ |
| getToken                      | 获取会话令牌                                 | String   | 1.0.0+ |
| isLogin                       | 判断会话是否处于登录态                       | Boolean  | 1.0.0+ |
| isAdmin                       | 判断登录用户是否为管理员                     | Boolean  | 1.0.0+ |
| hasRole(String roleKey)       | 判断用户是否拥有角色标识                     | Boolean  | 1.0.0+ |
| hasPerm(String perm)          | 判断用户是否拥有权限标识                     | Boolean  | 1.0.0+ |
| getRoles                      | 获取用户拥有角色标识列表                     | List     | 1.0.0+ |
| getPerms                      | 获取用户拥有权限标识列表                     | List     | 1.0.0+ |
| doLogin(Object id)            | 登录操作                                     | Boolean  | 1.0.0+ |
| doLogout(Object id)           | 登出操作，不传递id默认登出当前登录用户       | Boolean  | 1.0.0+ |
| doBan(Object id,Integer hour) | 封禁操作，封禁账户id时长*i**d*时长{hour}小时 | Void     | 1.0.0+ |
| isBan(Object id)              | 查询${id}是否处于封禁状态                    | Boolean  | 1.0.0+ |
| getDeptId()                   | 获取当前用户部门ID（不存在返回空）           | String   | 1.0.3+ |
| getRoleIds()                  | 获取当前用户拥有的角色ID列表                 | List     | 1.0.3+ |



### 缓存服务

#### 使用条件

1. 需完成`Redis`相关配置

#### 服务使用

配置完成后，只需要在使用类中引入

```java
@Autowried
RedisCache redisCache;
```

#### 方法列表

| 方法                                                         | 功能                       | 返回值  | 备注     |
| :----------------------------------------------------------- | :------------------------- | :------ | :------- |
| setCacheObject(final String key, final T value)              | 设置缓存                   | void    |          |
| setCacheObject(final String key, final T value, final Integer timeout, final TimeUnit timeUnit) | 设置定时缓存               | void    |          |
| expire(final String key, final long timeout)                 | 设置过期时间,默认单位（s） | boolean |          |
| expire(final String key, final long timeout, final TimeUnit unit) | 设置过期时间               | boolean |          |
| getCacheObject(final String key)                             | 查询缓存信息               | object  | 支持泛型 |
| deleteObject(final String key)                               | 删除缓存信息               | object  |          |
| deleteObject(final Collection collection)                    | 批量删除缓存信息           | void    |          |
| keys(final String pattern)                                   | 获取符合前缀的缓存key列表  | list    |          |

完整方法查阅源代码:

[RedisCache](https://gitee.com/lboot/lucy-spring-boot-starter/blob/master/src/main/java/org/lboot/starter/redis/RedisCache.java)



## 自定义注解

### @AuthLogin

> 校验用户是否登录

> 依赖于鉴权服务

### @CheckRole

> 角色核验

> 依赖于鉴权服务

| 参数   | 类型   | 说明     | 默认值 |
| ------ | ------ | -------- | ------ |
| value  | string | 角色标识 | admin  |
| orPerm | string | 权限标识 |        |

该注解用于方法拦截，当前登录用户拥有`value`对应的角色标识**或**拥有`orPerm`对应的权限，即可继续执行该方法，否则该方法会被拦截。

### @CheckPerm

> 权限核验

> 依赖于鉴权服务

| 参数   | 类型   | 说明     | 默认值 |
| ------ | ------ | -------- | ------ |
| value  | string | 权限标识 | *      |
| orRole | string | 角色标识 |        |

该注解用于方法拦截，当前登录用户拥有`value`对应的权限**或**拥有`orRole`对应的角色，即可继续执行该方法，否则该方法会被拦截。

### @FormLock

> 表单重复提交拦截

| 参数  | 类型   | 说明             | 默认值 |
| ----- | ------ | ---------------- | ------ |
| key   | string | 锁定key 别名     | ALL    |
| value | string | 锁定key 别名     | ALL    |
| sec   | int    | 锁定时长 单位 秒 | 5      |

该注解会优先从登录ID和key判断进行拦截，如果没有登录，会从IP和key进行拦截限制判定。

基础用法

```java
@FormLock("rbac-reg")
public ResponseDTO<Object> demoMethod(@RequestBody @Validated DemoParams params){
   // todo
}
```



### @RsrTree

> 资源权限生成

!> 1.0.3 版本之后支持

基于 `Swagger2`相关注解和 `@CheckRole`，`@CheckPermi`注解实现功能自定生成权限树，只需要在对应的 `Controller`上进行标记，即可自动生成权限树，可以通过`AuthRsrService`进行访问。



```java
@Slf4j
@RestController
@RequestMapping("system")
@AllArgsConstructor
@Api(tags = "部门管理")
@RsrTree("系统管理")
public class DeptController {
    SysDeptService deptService;
    xxxx
}
```

之后就可以通过`AuthRsrService`引入调用方法获取资源树信息。

- `rsrTree(groups)` 根据传入分组查询
- `rsrTree()`获取全部资源树信息

### @RsrIgnore

> 资源权限忽略

被该注解标识的方法，构建资源权限树过程中会忽略



## 更新日志

**1.1.2**

- 将`JPA`功能特性设置为拓展包
- `Redis`依赖排除，根据配置文件自动判定注入
- 移除`lucy-core`，`lucy-redis`库依赖

[更多...](https://gitee.com/lboot/lucy-spring-boot-starter/releases)

## 技术支持

<kindear@foxmail.com>
